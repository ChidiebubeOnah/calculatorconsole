﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorConsole
{
    internal static partial class Calculator
    {


        public static void Start()
        {
            try
            {
                Console.WriteLine("Calculator has started !\n\nHello What operation do you want to perform Press:\n1. Addition\n2. Subtraction\n3. Division\n4. Multiplication\n5. Modulus\nPress 0 to exit !");

                var userInput =  Console.ReadLine();

                switch (userInput)
                {
                    case "1":
                        Console.WriteLine("Enter the numbers you want to add in this format (1,2,4....,n): and press and enter to see the result:");
                        userInput = Console.ReadLine();
                        Operators.Addition(userInput);
                        break;
                    case "2":
                        Console.WriteLine("Enter the numbers you want to add in this format (1,2,4....,n): and press and enter to see the result:");
                        userInput = Console.ReadLine();
                        Operators.Subtraction(userInput);
                        break;
                    case "3":
                        Console.WriteLine("Division");
                        break;
                    case "4":
                        Console.WriteLine("Multiplication");
                        break;
                    case "5":
                        Console.WriteLine("Modulus");
                        break;
                    case "0":
                        Stop();
                        break;
                    default:
                        Console.WriteLine("Ops! your input is invalid!!!");
                        break;

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

           

        }

      

        private static void Stop()
        {
            Console.WriteLine("Calculator has stop !");
        }
    }
}
