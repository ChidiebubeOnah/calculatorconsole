﻿namespace CalculatorConsole;

internal static partial class Calculator
{
    private static class Operators
    {

        public static void Addition(string userInput)
        {

            if (string.IsNullOrWhiteSpace(userInput))
            {
                throw new InvalidOperationException("Blank Values are not allowed !");
            }

            var inputArray = userInput.Split(',');

            double total = default;

            foreach (var input in inputArray)
            {
               var canConvert = double.TryParse(input, out var value);

               if (canConvert)
               {
                   total += value;
               }
               else
               {
                   throw new InvalidOperationException("The numbers to add contain invalid digits  !");
               }
            }

            Console.WriteLine(total);
        }

        public static void Subtraction(string userInput)
        {

            if (string.IsNullOrWhiteSpace(userInput))
            {
                throw new InvalidOperationException("Blank Values are not allowed !");
            }

            var inputArray = userInput.Split(',');

            if (inputArray.Length == 1)
            {
                Console.WriteLine(inputArray[0]);
                return;
            }

            double total = default;

            foreach (var input in inputArray)
            {
                var canConvert = double.TryParse(input, out var value);

                if (canConvert)
                {
                    total -= value;
                }
                else
                {
                    throw new InvalidOperationException("The numbers to add contain invalid digits  !");
                }
            }

            Console.WriteLine(total);
        }

        public static void Multiplication()
        {

        }


        public static void Division()
        {

        }

        public static void Modulus()
        {

        }


    }
}